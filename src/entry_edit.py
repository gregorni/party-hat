# entry_edit.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import shutil
import uuid
from os import path

import PIL
from gi.repository import Adw, Gdk, GLib, Gtk
from PIL import Image, ImageOps

from . import file_chooser, set_avatar
from .saving import IMAGES_DIR


@Gtk.Template(resource_path="/io/gitlab/gregorni/PartyHat/gtk/entry-edit.ui")
class EntryEditWindow(Adw.Window):
    __gtype_name__ = "EntryEditWindow"

    toast_overlay = Gtk.Template.Child()
    cancel_btn = Gtk.Template.Child()
    save_btn = Gtk.Template.Child()
    name_row = Gtk.Template.Child()
    avatar = Gtk.Template.Child()
    change_avt_btn = Gtk.Template.Child()
    delete_avt_btn = Gtk.Template.Child()
    birth_date_row = Gtk.Template.Child()
    birth_date_btn = Gtk.Template.Child()
    birth_date_cal = Gtk.Template.Child()

    def __init__(self, parent_window, finish_function, data=None, **kwargs):
        super().__init__(**kwargs)

        self.set_transient_for(parent_window)

        self.save_btn.connect("clicked", lambda *_: self.__save(finish_function))
        self.cancel_btn.connect("clicked", self.__cancel)
        self.name_row.connect("changed", self.__check_is_filled)
        self.birth_date_row.connect("activated", self.__open_calendar)
        self.birth_date_cal.connect("day-selected", self.__set_birth_date_row_subtitle)
        self.change_avt_btn.connect(
            "clicked",
            lambda *_: file_chooser.choose_image(self, self.__on_file_selected),
        )
        self.delete_avt_btn.connect("clicked", self.__on_avatar_delete)

        self.__set_birth_date_row_subtitle()

        self.filepath = None

        self.return_data = {
            "id": str(uuid.uuid4().hex),
            "favourite": False,
        }

        if data:
            self.set_title(_("Edit Entry"))
            self.name_row.set_text(data["name"])
            self.birth_date_cal.select_day(
                GLib.DateTime.new_from_iso8601(data["birth-date-long"])
            )
            self.__on_file_selected(data["image"], True)

            self.return_data.update(data)

    def __open_calendar(self, *args):
        self.birth_date_btn.activate()

    def __check_is_filled(self, *args):
        self.save_btn.set_sensitive(self.name_row.get_text() != "")
        self.avatar.set_text(self.name_row.get_text())

    def __set_birth_date_row_subtitle(self, *args):
        self.birth_date_long = self.birth_date_cal.get_date().format_iso8601()
        self.birth_date_short = self.birth_date_long[:10]

        self.birth_date_row.set_subtitle(self.birth_date_short)

    def __on_file_selected(self, filepath, already_saved=False):
        if not already_saved:
            try:
                Gdk.Texture.new_from_filename(filepath)

                with Image.open(filepath) as img:
                    image_format = img.format
                    filepath = f"{IMAGES_DIR / self.return_data['id']}.{image_format}"
                    ImageOps.exif_transpose(img).save(filepath, format=image_format)

            except GLib.GError:
                self.toast_overlay.add_toast(
                    Adw.Toast.new(
                        _("{basename} is not a valid image").format(
                            basename=path.basename(filepath)
                        )
                    )
                )
                return

            except PIL.UnidentifiedImageError:
                new_filepath = f"{IMAGES_DIR / self.return_data['id']}.{path.splitext(filepath)[-1]}"
                shutil.copyfile(filepath, new_filepath)
                filepath = new_filepath

        self.filepath = filepath
        set_avatar.set_avatar(self.avatar, filepath)
        self.delete_avt_btn.set_sensitive(filepath is not None)

    def __on_avatar_delete(self, *args):
        self.__on_file_selected(None, True)

    def __cancel(self, *args):
        self.destroy()

    def __save(self, finish_function):
        self.return_data.update(
            {
                "name": self.name_row.get_text(),
                "birth-date-short": self.birth_date_short,
                "birth-date-long": self.birth_date_long,
                "image": self.filepath,
            }
        )
        finish_function(self.return_data)
        self.destroy()
