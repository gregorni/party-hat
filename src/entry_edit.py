# entry_edit.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import locale
import shutil
import uuid
from datetime import date
from os import path

import PIL
from gi.repository import Adw, Gdk, GLib, Gtk
from PIL import Image, ImageOps

from . import file_chooser, set_avatar
from .saving import IMAGES_DIR


@Gtk.Template(resource_path="/io/gitlab/gregorni/PartyHat/gtk/entry-edit.ui")
class EntryEditDialog(Adw.Dialog):
    __gtype_name__ = "EntryEditDialog"

    ctrl_enter_controller = Gtk.Template.Child()
    toast_overlay = Gtk.Template.Child()
    save_btn = Gtk.Template.Child()
    name_row = Gtk.Template.Child()
    avatar = Gtk.Template.Child()
    change_avt_btn = Gtk.Template.Child()
    delete_avt_btn = Gtk.Template.Child()
    birth_date_row = Gtk.Template.Child()
    birth_date_btn = Gtk.Template.Child()
    birth_date_cal = Gtk.Template.Child()

    def __init__(self, finish_function, data=None, **kwargs):
        super().__init__(**kwargs)

        self.ctrl_enter_controller.connect("key-released", self.__handle_ctrl_enter)

        self.save_btn.connect("clicked", lambda *args: self.__save(finish_function))
        self.name_row.connect("changed", self.__check_is_filled)
        self.birth_date_cal.connect("day-selected", self.__set_birth_date_row_subtitle)

        select_image = lambda *args: file_chooser.choose_image(
            self.get_root(), self.__on_file_selected
        )
        self.change_avt_btn.connect("clicked", select_image)

        unset_avatar = lambda *args: self.__on_file_selected(None, True)
        self.delete_avt_btn.connect("clicked", unset_avatar)

        self.__set_birth_date_row_subtitle()

        self.filepath = None

        randomized_identifier = str(uuid.uuid4().hex)
        self.return_data = {
            "id": randomized_identifier,
            "favourite": False,
        }

        if data == None:
            return

        self.set_title(_("Edit Entry"))
        self.name_row.set_text(data["name"])

        glib_birth_date = GLib.DateTime.new_from_iso8601(
            f"{data['birth-date']} 000000", GLib.TimeZone.new_local()
        )
        self.birth_date_cal.select_day(glib_birth_date)

        self.__on_file_selected(data["image"], True)

        self.return_data.update(data)

    def __handle_ctrl_enter(
        self, controller, key_pressed, keycode, current_modifiers, *args
    ):
        allowed_modifier_states = (
            Gdk.ModifierType.CONTROL_MASK,
            (Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.LOCK_MASK),
        )
        ctrl_is_held = current_modifiers in allowed_modifier_states

        correct_key_combo = key_pressed == Gdk.KEY_Return and ctrl_is_held

        if correct_key_combo and self.save_btn.get_sensitive():
            self.save_btn.activate()

    def __check_is_filled(self, *args):
        self.name = self.name_row.get_text().strip()

        self.save_btn.set_sensitive(self.name != "")
        self.avatar.set_text(self.name)

    def __set_birth_date_row_subtitle(self, *args):
        self.birth_date = self.birth_date_cal.get_date().format_iso8601()[:10]

        local_date_locale = locale.nl_langinfo(locale.D_FMT)
        formatted_date = date.fromisoformat(self.birth_date).strftime(local_date_locale)
        self.birth_date_row.set_subtitle(formatted_date)

    def __on_file_selected(self, filepath, already_saved=False):
        if not already_saved:
            try:
                Gdk.Texture.new_from_filename(filepath)

            except GLib.GError:
                message = _("File is not a valid image")
                self.toast_overlay.add_toast(Adw.Toast.new(message))
                return

            try:
                with Image.open(filepath) as img:
                    filepath = f"{IMAGES_DIR / self.return_data['id']}.{img.format}"

                    shrunken_image = ImageOps.fit(img, (200, 200))
                    transposed_image = ImageOps.exif_transpose(shrunken_image)
                    transposed_image.save(filepath, format=img.format)

            except PIL.UnidentifiedImageError:
                new_filepath = f"{IMAGES_DIR / self.return_data['id']}.{path.splitext(filepath)[-1]}"
                shutil.copyfile(filepath, new_filepath)
                filepath = new_filepath

        self.filepath = filepath
        set_avatar.set_avatar(self.avatar, filepath)
        self.delete_avt_btn.set_sensitive(filepath != None)

    def __save(self, finish_function):
        new_data = {
            "name": self.name,
            "birth-date": self.birth_date,
            "image": self.filepath,
        }
        self.return_data.update(new_data)
        finish_function(self.return_data)

        self.close()
