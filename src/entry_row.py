# entry_row.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import date

from gi.repository import Adw, Gtk

from . import get_days_until_birthday, set_avatar
from .entry_view import EntryViewPage
from .saving import Saving


@Gtk.Template(resource_path="/io/gitlab/gregorni/PartyHat/gtk/entry-row.ui")
class EntryRow(Adw.ActionRow):
    __gtype_name__ = "EntryRow"

    days_left_label = Gtk.Template.Child()
    avatar = Gtk.Template.Child()

    def __init__(self, parent_window, data, **kwargs):
        super().__init__(**kwargs)

        self.parent_window = parent_window
        self.data = data

        self.populate_labels(data)

        self.connect("activated", self.__view_entry)

    def __view_entry(self, *args):
        self.parent_window.navigation_view.push(EntryViewPage(self, self.data))

    def populate_labels(self, data):
        self.data = data

        self.set_title(data["name"])
        self.avatar.set_text(data["name"])
        set_avatar.set_avatar(self.avatar, data["image"])

        self.days_left = get_days_until_birthday.get_days(
            date.fromisoformat(data["birth-date-short"])
        )

        # Translators: Do not translate "{days_left}"!
        days_left_string = _("{days_left} days left").format(days_left=self.days_left)
        if self.days_left == 0:
            days_left_string = _("Today!")
        elif self.days_left == 1:
            days_left_string = _("Tomorrow")
        self.days_left_label.set_label(days_left_string)

    def on_delete(self, *args):
        self.parent_window.people_list_preferences_group.remove(self)
        self.parent_window.navigation_view.pop()

        Saving.delete(self.data)

        def undo(*args):
            Saving.save(self.data)
            self.parent_window.people_list_preferences_group.add(self)
            self.parent_window.first_page_stack.set_visible_child_name("people-list")

        # Translators: Do not translate "{name}"!
        toast = Adw.Toast.new(_('Deleted "{name}"').format(name=self.data["name"]))
        toast.set_button_label(_("Undo"))
        toast.connect("button-clicked", undo)
        self.parent_window.toast_overlay.add_toast(toast)

        if len(Saving.load()) == 0:
            self.parent_window.first_page_stack.set_visible_child_name("welcome")
