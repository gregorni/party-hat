# window.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw, Gio, Gtk

from .entry_edit import EntryEditDialog
from .entry_row import EntryRow
from .saving import Saving


@Gtk.Template(resource_path="/io/gitlab/gregorni/PartyHat/gtk/window.ui")
class PartyHatWindow(Adw.ApplicationWindow):
    __gtype_name__ = "PartyHatWindow"

    toast_overlay = Gtk.Template.Child()
    first_page_stack = Gtk.Template.Child()
    people_list_preferences_group = Gtk.Template.Child()
    add_entry_btn_welcome = Gtk.Template.Child()
    add_entry_btn = Gtk.Template.Child()
    navigation_view = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        settings = Gio.Settings(schema_id="io.gitlab.gregorni.PartyHat")
        bind_flags = Gio.SettingsBindFlags.DEFAULT
        settings.bind("window-width", self, "default-width", bind_flags)
        settings.bind("window-height", self, "default-height", bind_flags)
        settings.bind("window-is-maximized", self, "maximized", bind_flags)

        self.add_entry_btn_welcome.connect("clicked", self.on_new_entry)
        self.add_entry_btn.connect("clicked", self.on_new_entry)

        self.load_entries()

    def load_entries(self, delete_images=True):
        self.people_list_preferences_group.remove_all()

        entries = Saving.load(delete_images)
        for entry in entries:
            self.people_list_preferences_group.append(EntryRow(self, entry))

        page_to_show = "people-list" if len(entries) > 0 else "welcome"
        self.first_page_stack.set_visible_child_name(page_to_show)

    def on_new_entry(self, *args):
        def on_edit_entry_closed(data):
            Saving.save(data)
            self.load_entries()

        entry_dialog = EntryEditDialog(on_edit_entry_closed)
        entry_dialog.present(self)
