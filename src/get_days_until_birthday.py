# get_days_until_birthday.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


from datetime import date


def get_days(birthday):
    today = date.today()
    this_year = today.year

    days_until_birthday = lambda year: (
        date(year, birthday.month, birthday.day) - today
    ).days

    days_until_birthday_this_year = days_until_birthday(this_year)

    if days_until_birthday_this_year < 0:
        return days_until_birthday(this_year + 1)

    return days_until_birthday_this_year
