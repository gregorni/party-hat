# get_zodiacs_for_date.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import datetime

import zodiac_sign
from CnyZodiac import ChineseNewYearZodiac

from .translated_zodiacs_lists import ASIAN_ZODIACS, WESTERN_ZODIACS


def get_zodiacs(date):
    # Localization in the zodiac modules has to be overridden so that
    # the translation of the various signs is handled in Party Hat itself

    western_zodiac = zodiac_sign.get_zodiac_sign(date, language="en_US")
    translated_western_zodiac = WESTERN_ZODIACS[western_zodiac]

    same_format = "%Y%m%d"
    date_with_time = datetime.strptime(date.strftime(same_format), same_format)

    asian_zodiac = ChineseNewYearZodiac(lang="en").zodiac_date(date_with_time)
    translated_asian_zodiac = ASIAN_ZODIACS[asian_zodiac]

    return translated_western_zodiac, translated_asian_zodiac
