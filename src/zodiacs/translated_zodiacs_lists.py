# translated_zodiacs_lists.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


ASIAN_ZODIACS = {
    # Translators: This is an Asian zodiac sign
    "Monkey": _("Monkey"),
    # Translators: This is an Asian zodiac sign
    "Rooster": _("Rooster"),
    # Translators: This is an Asian zodiac sign
    "Dog": _("Dog"),
    # Translators: This is an Asian zodiac sign
    "Pig": _("Pig"),
    # Translators: This is an Asian zodiac sign
    "Rat": _("Rat"),
    # Translators: This is an Asian zodiac sign
    "Ox": _("Ox"),
    # Translators: This is an Asian zodiac sign
    "Tiger": _("Tiger"),
    # Translators: This is an Asian zodiac sign
    "Rabbit": _("Rabbit"),
    # Translators: This is an Asian zodiac sign
    "Dragon": _("Dragon"),
    # Translators: This is an Asian zodiac sign
    "Snake": _("Snake"),
    # Translators: This is an Asian zodiac sign
    "Horse": _("Horse"),
    # Translators: This is an Asian zodiac sign
    "Goat": _("Goat"),
}

WESTERN_ZODIACS = {
    # Translators: This is an Western zodiac sign
    "Aries": _("Aries"),
    # Translators: This is an Western zodiac sign
    "Taurus": _("Taurus"),
    # Translators: This is an Western zodiac sign
    "Gemini": _("Gemini"),
    # Translators: This is an Western zodiac sign
    "Cancer": _("Cancer"),
    # Translators: This is an Western zodiac sign
    "Leo": _("Leo"),
    # Translators: This is an Western zodiac sign
    "Virgo": _("Virgo"),
    # Translators: This is an Western zodiac sign
    "Libra": _("Libra"),
    # Translators: This is an Western zodiac sign
    "Scorpio": _("Scorpio"),
    # Translators: This is an Western zodiac sign
    "Sagittarius": _("Sagittarius"),
    # Translators: This is an Western zodiac sign
    "Capricorn": _("Capricorn"),
    # Translators: This is an Western zodiac sign
    "Aquarius": _("Aquarius"),
    # Translators: This is an Western zodiac sign
    "Pisces": _("Pisces"),
}
