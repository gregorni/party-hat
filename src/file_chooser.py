# file_chooser.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


def choose_image(parent, on_response_function):
    def on_response(dialog, response):
        """Run if the user selects a file."""
        if response == Gtk.ResponseType.ACCEPT:
            on_response_function(dialog.get_file().get_path())

    file_dialog = Gtk.FileChooserNative.new(
        title=_("Select a file"), parent=parent, action=Gtk.FileChooserAction.OPEN
    )

    file_dialog.set_modal(True)
    file_dialog.connect("response", on_response)

    file_filter = Gtk.FileFilter.new()
    file_filter.set_name(_("Supported image files"))
    file_filter.add_mime_type("image/png")
    file_filter.add_mime_type("image/jpeg")
    file_filter.add_mime_type("image/bmp")
    file_filter.add_mime_type("image/avif")
    file_filter.add_mime_type("image/gif")
    file_filter.add_mime_type("image/tiff")
    file_filter.add_mime_type("image/svg+xml")
    file_dialog.add_filter(file_filter)

    file_dialog.show()
