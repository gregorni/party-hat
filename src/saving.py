# saving.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


import json
import os
from datetime import date
from pathlib import Path

from . import get_days_until_birthday

DATA_DIR = Path(os.getenv("XDG_DATA_HOME"))
ENTRIES_DIR = DATA_DIR / "birthdays"
IMAGES_DIR = DATA_DIR / "images"


class Saving:
    ENTRIES_DIR.mkdir(parents=True, exist_ok=True)
    IMAGES_DIR.mkdir(parents=True, exist_ok=True)

    def save(data):
        json.dump(
            data,
            (ENTRIES_DIR / f"{data['id']}.json").open("w"),
            indent=4,
            sort_keys=True,
        )

    def edit_save(data):
        json.dump(
            data,
            (ENTRIES_DIR / f"{data['id']}.json").open("w"),
            indent=4,
            sort_keys=True,
        )

    def delete(data):
        os.remove(ENTRIES_DIR / f"{data['id']}.json")

    def load():
        entries = []

        if ENTRIES_DIR.is_dir():
            for open_file in ENTRIES_DIR.iterdir():
                entries.append(json.load(open_file.open()))

        entries.sort(
            key=lambda entry: (
                get_days_until_birthday.get_days(
                    date.fromisoformat(entry["birth-date-short"])
                )
            )
        )

        return entries
