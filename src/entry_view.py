# entry_view.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import date

from dateutil import relativedelta
from gi.repository import Adw, Gtk

from . import set_avatar
from .entry_edit import EntryEditWindow
from .saving import Saving


@Gtk.Template(resource_path="/io/gitlab/gregorni/PartyHat/gtk/entry-view.ui")
class EntryViewPage(Adw.NavigationPage):
    __gtype_name__ = "EntryViewPage"

    edit_btn = Gtk.Template.Child()
    delete_btn = Gtk.Template.Child()
    avatar = Gtk.Template.Child()
    favourite_btn = Gtk.Template.Child()
    name_label = Gtk.Template.Child()
    birth_date_row = Gtk.Template.Child()
    days_left_row = Gtk.Template.Child()
    current_age_row = Gtk.Template.Child()

    def __init__(self, parent, data, **kwargs):
        super().__init__(**kwargs)

        self.parent = parent
        self.data = data

        self.__populate_widgets(data)

        self.favourite_btn.connect("clicked", lambda *_: self.__update_favourite(data))

        self.edit_btn.connect("clicked", lambda *_: self.__on_edit(self.data))
        self.delete_btn.connect("clicked", parent.on_delete)

    def __update_favourite(self, data):
        data["favourite"] = not data["favourite"]
        Saving.edit_save(data)
        self.favourite_btn.set_icon_name(
            "non-starred-symbolic"
            if self.favourite_btn.get_icon_name() == "starred-symbolic"
            else "starred-symbolic"
        )

    def __on_edit(self, data):
        EntryEditWindow(
            self.parent.parent_window, self.__on_edit_finished, data=data
        ).present()

    def __on_edit_finished(self, data):
        Saving.edit_save(data)
        self.parent.populate_labels(data)
        self.__populate_widgets(data)

    def __populate_widgets(self, data):
        self.data = data
        self.set_title(data["name"])
        self.avatar.set_text(data["name"])
        set_avatar.set_avatar(self.avatar, data["image"])
        self.name_label.set_label(data["name"])

        birth_date_str = data["birth-date-short"]

        self.birth_date_row.set_subtitle(birth_date_str)
        self.days_left_row.set_subtitle(f"{self.parent.days_left}")
        self.current_age_row.set_subtitle(
            f"{relativedelta.relativedelta(date.today(), date.fromisoformat(birth_date_str)).years}"
        )

        self.favourite_btn.set_icon_name(
            "starred-symbolic" if data["favourite"] else "non-starred-symbolic"
        )
