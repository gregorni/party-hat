# entry_view.py
#
# Copyright 2023 Party Hat Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import locale
from datetime import date

from dateutil import relativedelta
from gi.repository import Adw, Gtk

from . import set_avatar
from .entry_edit import EntryEditDialog
from .saving import Saving
from .zodiacs import get_zodiacs_for_date


@Gtk.Template(resource_path="/io/gitlab/gregorni/PartyHat/gtk/entry-view.ui")
class EntryViewPage(Adw.NavigationPage):
    __gtype_name__ = "EntryViewPage"

    edit_btn = Gtk.Template.Child()
    delete_btn = Gtk.Template.Child()
    avatar = Gtk.Template.Child()
    favourite_btn = Gtk.Template.Child()
    name_label = Gtk.Template.Child()
    birth_date_row = Gtk.Template.Child()
    days_left_row = Gtk.Template.Child()
    current_age_row = Gtk.Template.Child()
    western_zodiac_row = Gtk.Template.Child()
    asian_zodiac_row = Gtk.Template.Child()

    def __init__(self, parent_row, data, **kwargs):
        super().__init__(**kwargs)

        self.parent_row = parent_row
        self.data = data

        self.__populate_widgets(data)

        self.favourite_btn.connect(
            "clicked", lambda *args: self.__update_favourite(data)
        )

        self.edit_btn.connect("clicked", self.__new_entry_window)
        self.delete_btn.connect("clicked", parent_row.on_delete)

    def __new_entry_window(self, *args):
        EntryEditDialog(self.__on_edit_finished, data=self.data).present(
            self.parent_row.parent_window
        )

    def __update_favourite(self, data):
        data["favourite"] = not data["favourite"]
        Saving.save(data)

        self.__set_fav_btn(data["favourite"])

    def __set_fav_btn(self, starred):
        icon_to_set = "non-starred-symbolic"
        if starred:
            icon_to_set = "starred-symbolic"
        self.favourite_btn.set_icon_name(icon_to_set)

    def __on_edit_finished(self, data):
        Saving.save(data)
        self.parent_row.populate_labels(data)
        self.__populate_widgets(data)

    def __populate_widgets(self, data):
        self.data = data
        self.set_title(data["name"])
        self.avatar.set_text(data["name"])
        set_avatar.set_avatar(self.avatar, data["image"])
        self.name_label.set_label(data["name"])

        birth_date = date.fromisoformat(data["birth-date"])

        localized_birth_date = birth_date.strftime(locale.nl_langinfo(locale.D_FMT))
        self.birth_date_row.set_subtitle(localized_birth_date)

        self.days_left_row.set_subtitle(f"{self.parent_row.days_left}")

        current_age = f"{relativedelta.relativedelta(date.today(), birth_date).years}"
        self.current_age_row.set_subtitle(current_age)

        self.__set_fav_btn(data["favourite"])

        western_zodiac, asian_zodiac = get_zodiacs_for_date.get_zodiacs(birth_date)
        self.western_zodiac_row.set_subtitle(western_zodiac)
        self.asian_zodiac_row.set_subtitle(asian_zodiac)
